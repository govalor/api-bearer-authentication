﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIBearerTest
{
    class AgileTask
    {
        public class ZnwWorkflowInstance
        {
            public string WorkflowName { get; set; }
            public object WorkflowType { get; set; }
            public string WorkflowTable { get; set; }
            public int WorkflowVersion { get; set; }
            public object WorkflowApprovalId { get; set; }
            public string WorkflowInstanceId { get; set; }
            public object WorkflowErrorObject { get; set; }
            public string WorkflowStateLookup { get; set; }
            public string WorkflowCurrentState { get; set; }
            public string WorkflowCurrentStateType { get; set; }
        }

        public class AppData
        {
            public string nwId { get; set; }
            public string Epic { get; set; }
            public string Task { get; set; }
            public int Sprint { get; set; }
            public int TaskID { get; set; }
            public string Assignee { get; set; }
            public int Estimate { get; set; }
            public string Reporter { get; set; }
            public string TaskSpec { get; set; }
            public string TaskType { get; set; }
            public string Component { get; set; }
            public string ScrumTeam { get; set; }
            public bool znwLocked { get; set; }
            public string AgileDesign { get; set; }
            public string StakeHolder { get; set; }
            public string SystemGroup { get; set; }
            public string AgileProject { get; set; }
            public string TaskPriority { get; set; }
            public List<object> AgileApproval { get; set; }
            public List<object> AgileComments { get; set; }
            public string ProductModule { get; set; }
            public string TargetVersion { get; set; }
            public string AffectedVersion { get; set; }
            public string DocumentationLink { get; set; }
            public ZnwWorkflowInstance znwWorkflowInstance { get; set; }
            public bool AgileIsUnplannedWork { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
            public List<object> UserInterfaceHints { get; set; }
        }

        public class PageData
        {
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
            public PageData pageData { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
}
