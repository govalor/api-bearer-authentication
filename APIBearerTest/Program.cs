﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;

namespace APIBearerTest
{
    class Program
    {
        public static string USERNAME =   "cheyanne.miller@nextworld.net";
        public static string PASSWORD = "Pion33rs!";
        //defaults
        public static string TOKEN_URL = "https://auth1.nextworld.net/Global/API/Authenticate/Tokens";
        public static string URL_BEGINNING = "https://master";
        public static string URL_BODY = "-api1.nextworld.net/v2/";
        public static string URL_TABLE = "AgileTasks?nwFilter=%7B%22$and%22:%5B%7B%22SystemGroup%22:%22Integrations%22%7D,%7B%22ProductModule%22:%22Integration%22%7D,%7B%22ScrumTeam%22:%22Integration%22%7D,%7B%22Sprint%22:51%7D,%7B%22Assignee%22:%7B%22$like%22:%22cheyanne.miller@nextworld.net%22%7D%7D%5D%7D"; //"JVAdminGetStudents";
        //public static string URL_TABLE = "CMSchoologyGroups";

        public static string ZONE = "AppStable";
        public static string LIFECYCLE = "base"; //"jake_v"; "cheyanne"
        public static string ACCESS_TOKEN = "";

        private static void getToken()
        {
            Token.RootObject root = new Token.RootObject();
            Authentication.RootObject postRoot = new Authentication.RootObject();

            //indicate desired zone and lifecycle
            postRoot.lifecycle = LIFECYCLE;
            postRoot.zone = ZONE;

            RestClient client = new RestClient(TOKEN_URL);
            client.Authenticator = new HttpBasicAuthenticator(USERNAME, PASSWORD);

            IRestRequest request = new RestRequest(Method.POST);
            request.AddParameter("application/json", JsonConvert.SerializeObject(postRoot), ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            root = JsonConvert.DeserializeObject<Token.RootObject>(response.Content);
            
            ACCESS_TOKEN = root.access_token;
        }

        private static NWGroupsModel.RootObject getGroups()
        {
            //get token to authorize access
            getToken();

            NWGroupsModel.RootObject root = new NWGroupsModel.RootObject();

            RestClient client = new RestClient(URL_BEGINNING + URL_BODY + URL_TABLE);
            //client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", ACCESS_TOKEN)); //DELETE
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            IRestRequest request = new RestRequest(Method.GET);

            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            root = JsonConvert.DeserializeObject<NWGroupsModel.RootObject>(response.Content);

            return root;
        }

        private static AgileTask.RootObject getTasks()
        {
            //get token to authorize access
            getToken();

            AgileTask.RootObject root = new AgileTask.RootObject();

            RestClient client = new RestClient(URL_BEGINNING + URL_BODY + URL_TABLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            IRestRequest request = new RestRequest(Method.GET);

            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<AgileTask.RootObject>(response.Content);

            return root;
        }

        private static StudentNW.RootObject getStudents()
        {
            //get token to authorize access
            getToken();

            StudentNW.RootObject root = new StudentNW.RootObject();

            RestClient client = new RestClient(URL_BEGINNING + URL_BODY + URL_TABLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            IRestRequest request = new RestRequest(Method.GET);

            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            root = JsonConvert.DeserializeObject<StudentNW.RootObject>(response.Content);

            return root;
        }

        private static StudentNW.RootObject studentsTest()
        {
            StudentNW.RootObject root = new StudentNW.RootObject();

            RestClient client = new RestClient(URL_BEGINNING + URL_BODY + URL_TABLE);
            client.Authenticator = new HttpBasicAuthenticator(USERNAME, PASSWORD);

            IRestRequest request = new RestRequest(Method.GET);

            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            root = JsonConvert.DeserializeObject<StudentNW.RootObject>(response.Content);

            return root;
        }

        private static void printGroups(NWGroupsModel.RootObject root)
        {
            foreach(NWGroupsModel.Record group in root.Data.records)
            {
                Console.WriteLine("Group title: " + group.appData.CMGroupTitle);
                Console.WriteLine("Group id: " + group.appData.CMGroupID);
                Console.WriteLine();
            }
            Console.ReadLine();
        }

        private static void printStudents(StudentNW.RootObject root)
        {
            foreach(StudentNW.Record student in root.Data.records)
            {
                Console.WriteLine("Name: " + student.appData.JVFirstName + " " + student.appData.JVLastName);
                Console.WriteLine("Grade: " + student.appData.JVGradeLevel);
                Console.WriteLine("Email: " + student.appData.JVEmail);
                Console.WriteLine();
            }
            Console.ReadLine();
        }

        private static void printTasks(AgileTask.RootObject root)
        {
            foreach(AgileTask.Record record in root.Data.records)
            {
                Console.WriteLine("Sprint: " + record.appData.Sprint);
                Console.WriteLine("Assignee: " + record.appData.Assignee);
                Console.WriteLine("Estimate: " + record.appData.Estimate);
                Console.WriteLine("Task Priority: " + record.appData.TaskPriority);
                Console.WriteLine();
            }
            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            //use retrieved token to access data
            //NWGroupsModel.RootObject root = getGroups();
            //StudentNW.RootObject root2 = getStudents();
            AgileTask.RootObject root3 = getTasks();

            //print retrieved data to view
            //printGroups(root);
            //printStudents(root2);
            printTasks(root3);
        }
    }
}
